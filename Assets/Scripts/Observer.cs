﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Checkers {
	public class Observer : MonoBehaviour, IObserver {
		[SerializeField]
		public bool _record, _replay;
		private IObservable _iObservable;

		public bool GetRecord => _record;
		public bool GetReplay => _replay;

		public event ReplayAction OnReplayAction;

		private void Start() {
			_iObservable = GetComponent<IObservable>();
			_iObservable.OnRecordAction += OnRecordAction;

			if(_replay) {
				//StartCoroutine(StartAction());
				OnReplayAction?.Invoke();
			}
		}

		private void OnDisable() {
			_iObservable.OnRecordAction -= OnRecordAction;
		}

		public void OnRecordAction(string message) {
			if(!_record) return;

			var path = Environment.CurrentDirectory + "\\Record.txt";

			if(!File.Exists(path)) {
				var file = File.Create(path);
				file.Close();
			}

			using(var sw = new StreamWriter(path, true)) {
				sw.WriteLine(message);
			}
		}

		IEnumerator StartAction() {
			yield return new WaitForSeconds(2f);

			OnReplayAction?.Invoke();
		}
	}

	public interface IObserver {
		event ReplayAction OnReplayAction;
		void OnRecordAction(string message);
	}

	public delegate void ReplayAction();
}