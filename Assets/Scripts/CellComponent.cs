﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Checkers {
    public class CellComponent : BaseClickComponent {
        private Dictionary<NeighborType, CellComponent> _neighbors;
        public bool CanMove = false;

        /// <summary>
        /// Возвращает соседа клетки по указанному направлению
        /// </summary>
        /// <param name="type">Перечисление направления</param>
        /// <returns>Клетка-сосед или null</returns>
        public CellComponent GetNeighbors(NeighborType type) => _neighbors[type];

		public override void OnPointerEnter(PointerEventData eventData) {
            CallBackEvent(this, true);
        }

        public override void OnPointerExit(PointerEventData eventData) {
            CallBackEvent(this, false);
        }

        /// <summary>
        /// Конфигурирование связей клеток
        /// </summary>
		public void Configuration(Dictionary<NeighborType, CellComponent> neighbors) {
            if(_neighbors != null) return;
            _neighbors = neighbors;
        }
		private void OnTriggerEnter(Collider other) {
            CellComponent cellComponent = other.GetComponent<CellComponent>();
            if(cellComponent) {
                Vector3 pos = other.transform.position;
                if(pos.x < transform.position.x && pos.z > transform.position.z) {
                    _neighbors[NeighborType.TopLeft] = cellComponent;
                }
                else if(pos.x > transform.position.x && pos.z > transform.position.z) {
                    _neighbors[NeighborType.TopRight] = cellComponent;
                }
                else if(pos.x < transform.position.x && pos.z < transform.position.z) {
                    _neighbors[NeighborType.BottomLeft] = cellComponent;
                }
                else if(pos.x > transform.position.x && pos.z < transform.position.z) {
                    _neighbors[NeighborType.BottomRight] = cellComponent;
                }
            }
        }
		private void OnTriggerExit(Collider other) {
            ChipComponent chipComponent = other.GetComponent<ChipComponent>();
			if(chipComponent && Pair == (BaseClickComponent)chipComponent) {
                Pair = null;
			}
        }
	}

    /// <summary>
    /// Тип соседа клетки
    /// </summary>
    public enum NeighborType : byte {
        /// <summary>
        /// Клетка сверху и слева от данной
        /// </summary>
        TopLeft,
        /// <summary>
        /// Клетка сверху и справа от данной
        /// </summary>
        TopRight,
        /// <summary>
        /// Клетка снизу и слева от данной
        /// </summary>
        BottomLeft,
        /// <summary>
        /// Клетка снизу и справа от данной
        /// </summary>
        BottomRight
    }
}