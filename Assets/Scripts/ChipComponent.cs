﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Checkers {
    public class ChipComponent : BaseClickComponent {
        public bool IsMoving = false, IsDeath = false;
        private Rigidbody _rb;

        public event DeathChip OnDeathChip;


        private new void Start() {
            _rb = GetComponent<Rigidbody>();
		}

		public override void OnPointerEnter(PointerEventData eventData) {
            CallBackEvent((CellComponent)Pair, true);
        }

        public override void OnPointerExit(PointerEventData eventData) {
            CallBackEvent((CellComponent)Pair, false);
        }

        private void OnTriggerStay(Collider other) {
            CellComponent cellComponent = other.GetComponent<CellComponent>();

            if(cellComponent != null) {
				if(Pair != null) {
                    Pair.Pair = null;
                }
                Pair = (BaseClickComponent)cellComponent;
                Pair.Pair = this;
            }
            else {
                Pair = null;
            }
        }
		private void OnTriggerEnter(Collider other) {
            ChipComponent chipComponent = other.GetComponent<ChipComponent>();

            if(chipComponent != null && _rb.IsSleeping() && GetColor != chipComponent.GetColor) {
                IsDeath = true;
                OnDeathChip.Invoke(Pair.gameObject.name);
                if(Pair != null) {
					if(Pair.Pair != null) {
                        Pair.Pair = null;
                    }
                    Pair = null;
                }
            }
        }
	}

    public delegate void DeathChip(string action);
}