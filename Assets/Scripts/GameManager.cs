﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using System;
using System.IO;
using System.Threading;

namespace Checkers {
	public class GameManager : MonoBehaviour, IObservable {
		private BaseClickComponent _baseClickComponent;
		[SerializeField]
		private List<ChipComponent> _chipComponents, _chipComponentsWhite, _chipComponentsBlack;
		[SerializeField]
		private List<CellComponent> _cellComponents;
		[SerializeField]
		private Material _clickMaterial, _hoverMaterial;
		private static bool _moveOfWhite = true;
		private ChipComponent _activeChip;
		[SerializeField]
		private GameObject _camera;
		[SerializeField]
		private float _moveSpeed, _rorateSpeed;
		[Tooltip("Pадержка между действиями в реплее"), SerializeField]
		private float _waitSeconds;
		private bool _isMoving = false, _isRecording = true, _isReplaying = false;
		private Observer _observer;

		public event RecordAction OnRecordAction;

		public static bool GetMoveOfWhite => _moveOfWhite;

		private void Start() {
			foreach(var item in _chipComponents) {
				item.OnClickEventHandler += OnClickChip;
				item.OnFocusEventHandler += OnFocus;
				item.OnDeathChip += OnDeathChip;
			}
			foreach(var item in _cellComponents) {
				item.OnClickEventHandler += OnClickCell;
				item.OnFocusEventHandler += OnFocus;
				item.Configuration(new Dictionary<NeighborType, CellComponent> {
					{ NeighborType.TopLeft, null },
					{ NeighborType.TopRight, null },
					{ NeighborType.BottomLeft, null },
					{ NeighborType.BottomRight, null }
				});
			}

			_observer = GetComponent<Observer>();
			if(_observer == null || !_observer.enabled || !_observer.GetRecord) {
				_isRecording = false;
			}
			if(_observer == null || _observer.GetReplay) {
				_isReplaying = true;
			}
			_observer.OnReplayAction += OnReplayAction;
		}

		private void OnDisable() {
			foreach(var item in _chipComponents) {
				item.OnClickEventHandler -= OnClickChip;
				item.OnFocusEventHandler -= OnFocus;
				item.OnDeathChip -= OnDeathChip;
			}
			foreach(var item in _cellComponents) {
				item.OnClickEventHandler -= OnClickCell;
				item.OnFocusEventHandler -= OnFocus;
			}
			_observer.OnReplayAction -= OnReplayAction;
		}

		private void OnClickChip(BaseClickComponent component) {
			if(_isMoving || _isReplaying) {
				return;
			}

			if(_moveOfWhite && component.GetColor == ColorType.White) {
				SelectChip((CellComponent)component.Pair, true);
			}
			else if(!_moveOfWhite && component.GetColor == ColorType.Black) {
				SelectChip((CellComponent)component.Pair, false);
			}
			else {
				if(_moveOfWhite) {
					Debug.Log("Сейчас ход белых!");
				}
				else {
					Debug.Log("Сейчас ход черных!");
				}
				return;
			}
		}
		private void OnFocus(CellComponent component, bool isSelect) {
			if(_isMoving || _isReplaying) {
				return;
			}

			if(component.GetColor == ColorType.White) {
				return;
			}
			if(isSelect) {
				component.AddAdditionalMaterial(_hoverMaterial, 1);
			}
			else {
				component.RemoveAdditionalMaterial(1);
			}
		}
		private void OnClickCell(BaseClickComponent component) {
			if(_isMoving || component.GetColor == ColorType.White || _isReplaying) {
				return;
			}

			if(_moveOfWhite && component.Pair != null && component.Pair.GetColor == ColorType.White) {
				SelectChip((CellComponent)component, true);
				return;
			}
			else if(!_moveOfWhite && component.Pair != null && component.Pair.GetColor == ColorType.Black) {
				SelectChip((CellComponent)component, false);
				return;
			}
			else {
				CellComponent cellComponent = (CellComponent)component;
				if(cellComponent.CanMove) {
					ClearMaterial();

					if(_isRecording) {
						string message;
						if(_moveOfWhite) {
							message = $"White: move chip to {cellComponent.gameObject.name}";
						}
						else {
							message = $"Black: move chip to {cellComponent.gameObject.name}";
						}
						OnRecordAction?.Invoke(message);
					}

					StartCoroutine(MakeMove(cellComponent.transform.position));
					return;
				}
				else {
					Debug.Log("Не выбрана фишка!");
					return;
				}
			}
		}
		private void SelectChip(CellComponent component, bool IsWhite) {
			ClearMaterial();

			if(_isRecording) {
				string message;
				if(IsWhite) {
					message = $"White: select chip in {component.gameObject.name}";
				}
				else {
					message = $"Black: select chip in {component.gameObject.name}";
				}
				OnRecordAction?.Invoke(message);
			}

			if(_activeChip == (ChipComponent)component.Pair) {
				_activeChip = null;
				return;
			}

			component.AddAdditionalMaterial(_clickMaterial, 2);
			CellComponent neighbor, neighbor2;
			if(IsWhite) {
				_activeChip = (ChipComponent)component.Pair;
				neighbor = component.GetNeighbors(NeighborType.TopLeft);
				if(neighbor != null && neighbor.Pair == null) {
					neighbor?.AddAdditionalMaterial(_clickMaterial, 2);
					neighbor.CanMove = true;
				}
				else if(neighbor != null && neighbor.Pair != null && neighbor.Pair.GetColor == ColorType.Black) {
					neighbor2 = neighbor.GetNeighbors(NeighborType.TopLeft);
					if(neighbor2 != null && neighbor2.Pair == null) {
						neighbor2?.AddAdditionalMaterial(_clickMaterial, 2);
						neighbor2.CanMove = true;
					}
				}

				neighbor = component.GetNeighbors(NeighborType.TopRight);
				if(neighbor != null && neighbor.Pair == null) {
					neighbor?.AddAdditionalMaterial(_clickMaterial, 2);
					neighbor.CanMove = true;
				}
				else if(neighbor != null && neighbor.Pair != null && neighbor.Pair.GetColor == ColorType.Black) {
					neighbor2 = neighbor.GetNeighbors(NeighborType.TopRight);
					if(neighbor2 != null && neighbor2.Pair == null) {
						neighbor2?.AddAdditionalMaterial(_clickMaterial, 2);
						neighbor2.CanMove = true;
					}
				}
			}
			else {
				_activeChip = (ChipComponent)component.Pair;
				neighbor = component.GetNeighbors(NeighborType.BottomLeft);
				if(neighbor != null && neighbor.Pair == null) {
					neighbor?.AddAdditionalMaterial(_clickMaterial, 2);
					neighbor.CanMove = true;
				}
				else if(neighbor != null && neighbor.Pair != null && neighbor.Pair.GetColor == ColorType.White) {
					neighbor2 = neighbor.GetNeighbors(NeighborType.BottomLeft);
					if(neighbor2 != null && neighbor2.Pair == null) {
						neighbor2?.AddAdditionalMaterial(_clickMaterial, 2);
						neighbor2.CanMove = true;
					}
				}

				neighbor = component.GetNeighbors(NeighborType.BottomRight);
				if(neighbor != null && neighbor.Pair == null) {
					neighbor?.AddAdditionalMaterial(_clickMaterial, 2);
					neighbor.CanMove = true;
				}
				else if(neighbor != null && neighbor.Pair != null && neighbor.Pair.GetColor == ColorType.White) {
					neighbor2 = neighbor.GetNeighbors(NeighborType.BottomRight);
					if(neighbor2 != null && neighbor2.Pair == null) {
						neighbor2?.AddAdditionalMaterial(_clickMaterial, 2);
						neighbor2.CanMove = true;
					}
				}
			}
		}
		private void ClearMaterial() {
			foreach(var item in _cellComponents) {
				item.RemoveAdditionalMaterial(1);
				item.RemoveAdditionalMaterial(2);
				item.CanMove = false;
			}
		}
		IEnumerator MakeMove(Vector3 target) {
			_isMoving = true;

			var chipComponent = _activeChip.GetComponent<ChipComponent>();
			chipComponent.IsMoving = true;
			Vector3 newTarget = new Vector3(target.x, _activeChip.transform.position.y, target.z);
			while(_activeChip.transform.position != newTarget) {
				_activeChip.transform.position = Vector3.MoveTowards(_activeChip.transform.position, newTarget, Time.deltaTime * _moveSpeed);
				yield return null;
			}
			DestroyChip();
			chipComponent.IsMoving = false;
			CheckWin();

			if(_camera.transform.rotation.eulerAngles.y < 180) {
				while(_camera.transform.rotation.eulerAngles.y < 180) {
					_camera.transform.Rotate(0, Time.deltaTime * _rorateSpeed, 0);
					yield return null;
				}
			}
			else {
				while(_camera.transform.rotation.eulerAngles.y > 180) {
					_camera.transform.Rotate(0, Time.deltaTime * _rorateSpeed, 0);
					yield return null;
				}
			}

			_moveOfWhite = !_moveOfWhite;
			_isMoving = false;
		}
		private void DestroyChip() {
			for(int i = 0; i < _chipComponents.Count; i++) {
				if(_chipComponents[i].IsDeath) {
					Destroy(_chipComponents[i].gameObject);
					_chipComponents.RemoveAt(i);
				}
			}
			_chipComponentsWhite = _chipComponentsWhite.Where(t => !t.IsDeath).ToList();
			_chipComponentsBlack = _chipComponentsBlack.Where(t => !t.IsDeath).ToList();
		}
		private void CheckWin() {
			bool winWhite = false, winBlack = false;
			if(_chipComponentsWhite.Count == 0) {
				winBlack = true;
			}
			if(_chipComponentsBlack.Count == 0) {
				winWhite = true;
			}
			if(!winWhite && !winBlack) {
				foreach(var item in _chipComponents) {
					if(item.transform.position.z == 3.5f && item.GetColor == ColorType.White) {
						winWhite = true;
						break;
					}
					if(item.transform.position.z == -3.5f && item.GetColor == ColorType.Black) {
						winBlack = true;
						break;
					}
				}
			}
			if(winWhite) {
				Debug.Log("Белые выйграли!");
				EditorApplication.isPaused = true;
			}
			if(winBlack) {
				Debug.Log("Черные выйграли!");
				EditorApplication.isPaused = true;
			}
		}
		private void OnDeathChip(string cellName) {
			OnRecordAction?.Invoke($"Remove chip in {cellName}");
		}
		public void OnReplayAction() {
			var path = Environment.CurrentDirectory + "\\Record.txt";

			if(!File.Exists(path)) {
				Debug.LogWarning("Нет файла Record.txt для воспроизведения!");
				EditorApplication.isPlaying = false;
				return;
			}

			StartCoroutine(ReplayActions(path));
		}
		IEnumerator ReplayActions(string path) {
			string line;
			GameObject cell;

			using(var sr = new StreamReader(path)) {
				while((line = sr.ReadLine()) != null) {
					line = line.ToUpper();
					cell = GameObject.Find(line.Substring(line.Length - 2));

					if(line.Contains("REMOVE")) {
						var chip = (ChipComponent)cell.GetComponent<CellComponent>().Pair;
						if(!chip.IsDeath) {
							chip.IsDeath = true;
						}
						continue;
					}

					yield return new WaitForSeconds(_waitSeconds);

					if(line.Contains("SELECT")) {
						SelectChip(cell.GetComponent<CellComponent>(), _moveOfWhite);
					}
					else if(line.Contains("MOVE")) {
						ClearMaterial();
						StartCoroutine(MakeMove(cell.transform.position));
					}

					DestroyChip();
					CheckWin();
				}
			}
		}
	}

	public interface IObservable {
		event RecordAction OnRecordAction;
		void OnReplayAction();
	}

    public delegate void RecordAction(string action);
}